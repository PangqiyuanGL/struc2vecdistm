# -*- coding: utf-8 -*-
"""
Created on Wed Apr  8 19:41:23 2020

@author: Qiyuan Pang

Modify data to get rid of two problems
"""


import json
import os

filepath = './train'
filenames = os.listdir(filepath)
N = len(filenames)
print(type(filenames))

for i in range(0,N):
    if filenames[i].find('edges') >= 0:
        edgefile = filepath + '/' + filenames[i]
        with open(edgefile, 'r') as f:
             edges = json.load(f)
        f.close()
        ind2del = []
        starts = []
        ends = []
        
        for j in range(0,len(edges)):
            if  edges[j]['start'] == edges[j]['end']:
                ind2del.append(j)
            start = edges[j]['start']
            end = edges[j]['end']
            starts.append(start)
            ends.append(end)
            
        for j in range(0,len(edges)):
            start = edges[j]['start']
            end = edges[j]['end']
            stind = [k for k in range(0,len(starts)) if starts[k]==start]
            edind = [k for k in range(0,len(starts)) if ends[k]==end]
            intsec = list(set(stind).intersection(set(edind)))
            if len(intsec) > 1:
                if  edges[intsec[0]]['drivable'] == 0:
                    ind2del.append(intsec[0])
                else:
                    ind2del.append(intsec[1])
        
        empty = [];
        ind2del = list(set(empty).union(set(ind2del)))
        ind2del.sort(reverse=True)
        for j in range(0,len(ind2del)):
            edges.pop(ind2del[j])
        os.remove(edgefile)
        file = open(edgefile,'w')
        json.dump(edges,file)
        file.close()
        
        
        
        
filepath = './test1'
filenames = os.listdir(filepath)
N = len(filenames)
print(type(filenames))  
for i in range(0,N):
    if filenames[i].find('edges') >= 0:
        edgefile = filepath + '/' + filenames[i]
        with open(edgefile, 'r') as f:
             edges = json.load(f)
        f.close()
        ind2del = []
        starts = []
        ends = []
        
        for j in range(0,len(edges)):
            if  edges[j]['start'] == edges[j]['end']:
                ind2del.append(j)
            start = edges[j]['start']
            end = edges[j]['end']
            starts.append(start)
            ends.append(end)
            
        for j in range(0,len(edges)):
            start = edges[j]['start']
            end = edges[j]['end']
            stind = [k for k in range(0,len(starts)) if starts[k]==start]
            edind = [k for k in range(0,len(starts)) if ends[k]==end]
            intsec = list(set(stind).intersection(set(edind)))
            if len(intsec) > 1:
                if  edges[intsec[0]]['drivable'] == 0:
                    ind2del.append(intsec[0])
                else:
                    ind2del.append(intsec[1])
        
        empty = [];
        ind2del = list(set(empty).union(set(ind2del)))
        ind2del.sort(reverse=True)
        for j in range(0,len(ind2del)):
            edges.pop(ind2del[j])
        os.remove(edgefile)
        file = open(edgefile,'w')
        json.dump(edges,file)
        file.close()
            
        
        
        
        
filepath = './test2'
filenames = os.listdir(filepath)
N = len(filenames)
print(type(filenames))  
for i in range(0,N):
    if filenames[i].find('edges') >= 0:
        edgefile = filepath + '/' + filenames[i]
        with open(edgefile, 'r') as f:
             edges = json.load(f)
        f.close()
        ind2del = []
        starts = []
        ends = []
        
        for j in range(0,len(edges)):
            if  edges[j]['start'] == edges[j]['end']:
                ind2del.append(j)
            start = edges[j]['start']
            end = edges[j]['end']
            starts.append(start)
            ends.append(end)
            
        for j in range(0,len(edges)):
            start = edges[j]['start']
            end = edges[j]['end']
            stind = [k for k in range(0,len(starts)) if starts[k]==start]
            edind = [k for k in range(0,len(starts)) if ends[k]==end]
            intsec = list(set(stind).intersection(set(edind)))
            if len(intsec) > 1:
                if  edges[intsec[0]]['drivable'] == 0:
                    ind2del.append(intsec[0])
                else:
                    ind2del.append(intsec[1])
        
        empty = [];
        ind2del = list(set(empty).union(set(ind2del)))
        ind2del.sort(reverse=True)
        for j in range(0,len(ind2del)):
            edges.pop(ind2del[j])
        os.remove(edgefile)
        file = open(edgefile,'w')
        json.dump(edges,file)
        file.close()
        
        
        
        
    
    
filepath = './validation'
filenames = os.listdir(filepath)
N = len(filenames)
print(type(filenames))  
for i in range(0,N):
    if filenames[i].find('edges') >= 0:
        edgefile = filepath + '/' + filenames[i]
        with open(edgefile, 'r') as f:
             edges = json.load(f)
        f.close()
        ind2del = []
        starts = []
        ends = []
        
        for j in range(0,len(edges)):
            if  edges[j]['start'] == edges[j]['end']:
                ind2del.append(j)
            start = edges[j]['start']
            end = edges[j]['end']
            starts.append(start)
            ends.append(end)
            
        for j in range(0,len(edges)):
            start = edges[j]['start']
            end = edges[j]['end']
            stind = [k for k in range(0,len(starts)) if starts[k]==start]
            edind = [k for k in range(0,len(starts)) if ends[k]==end]
            intsec = list(set(stind).intersection(set(edind)))
            if len(intsec) > 1:
                if  edges[intsec[0]]['drivable'] == 0:
                    ind2del.append(intsec[0])
                else:
                    ind2del.append(intsec[1])
        
        empty = [];
        ind2del = list(set(empty).union(set(ind2del)))
        ind2del.sort(reverse=True)
        for j in range(0,len(ind2del)):
            edges.pop(ind2del[j])
        os.remove(edgefile)
        file = open(edgefile,'w')
        json.dump(edges,file)
        file.close()
        
                

