# -*- coding: utf-8 -*-
"""
Created on Wed Apr  8 18:29:41 2020

@author: Qiyuan Pang
"""


import json
import os
import platform
import numpy as np
import networkx as nx
import matplotlib.pyplot as plt
from struc2vec import *
from sklearn.metrics import brier_score_loss, log_loss
from scipy.special import expit
from ampligraph.latent_features.models import DistMult
from ampligraph.evaluation.protocol import *
from auxiliaryfunc import *
#def testset(aset, asubset):

batchpos = 100
batchneg = 9*batchpos
epoch = 5
e = 0.7
lr = 0.0005
act = 'sigmoid'

## an example for how use createsamples in auxiliaryfunc.py to create data for training and testing
filepath = './train/sample' # change when necessary
filenames = os.listdir(filepath)
# just load n graphs in filepath
n = 20
# positive samples
Xpos = np.array([np.array([50]),'0',np.array([50])],dtype=object)
# negative samples
Xneg = np.array([np.array([50]),'0',np.array([50])],dtype=object)
# samples for test
Xtest = np.array([np.array([50]),'0',np.array([50])],dtype=object)
# edges for test
Etest = list()
count = 0
while len(filenames) > 0 and count < n:
    # load nodes and edges in a graph
    name = filenames[0][0:-12]
    nodesset = np.load((filepath+'/'+name+'nodesemb.npy'),allow_pickle=True)
    edgesset = np.load((filepath+'/'+name+'edgesemb.npy'),allow_pickle=True)
    # create embeddings for the graph
    [emb1, emb2, embn, embt1, embt2, edgest1, edgest2] = createsamples(nodesset,edgesset)
    # stack all corresponding data together
    Xpos = np.vstack((Xpos,emb1))
    Xneg = np.vstack((Xneg,embn))
    Xtest = np.vstack((Xtest,embt1))
    Etest = Etest + edgest1
    count = count + 1
    # remove the name of the graph after loading
    filenames.remove((name+'nodesemb.npy'))
    filenames.remove((name+'edgesemb.npy'))


Xpos = np.delete(Xpos,0,axis=0)
Xneg = np.delete(Xneg,0,axis=0)
Xtest = np.delete(Xtest,0,axis=0)

# initiate weights for training 
n = len(Xpos[0][0])
W0 = np.ones([n,n])
M10 = np.ones([n])
M20 = np.ones([n])
# call distmult
[W, M1, M2] = distmult(Xpos,Xneg,W0,M10,M20,lr,batchpos,batchneg,epoch)

pred = predict(M1,M2,W,Xtest,e)

print(pred)
'''
model = DistMult(batches_count=64, seed=0, epochs=50, k=100, eta=20, optimizer='adam', optimizer_params={'lr':0.0001}, loss='pairwise', verbose=True)

model.fit(X)

name = filenames[0][0:len(filenames[0])-4]

#filenames1 = os.listdir((filepath + '/sample')) 
#Y = np.load((filepath + '/sample/' + name + 'NEG.npy'),allow_pickle=True)
X_valid_pos = np.load((filepath + '/sample/' + filenames[0]),allow_pickle=True)
X_valid_neg = np.load((filepath + '/sample/' + name + 'NEG.npy'),allow_pickle=True)
X_test = np.load((filepath + '/sample/' + name + 'TEST.npy'),allow_pickle=True)

#model.calibrate(X_valid_pos, X_valid_neg, positive_base_rate=None)



#scores = model.predict(X_test,filter_unseen=True)
rank = evaluate_performance(X, model, filter_triples=None, verbose=False, filter_unseen=True)

print(rank)
'''
