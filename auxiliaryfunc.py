import numpy as np

def nodesosmid(nodes):
    N = len(nodes)
    osmid = list()
    for i in range(0,N):
        ids = str(nodes[i]['osmid'])
        osmid.append(ids)
    return osmid

def get_index(lst=None, item=''):
    #return [i for i in range(len(lst)) if lst[i] == item]
    ind = list()
    for i in range(len(lst)):
        if lst[i] == item:
            ind.append(i)
    return ind

def lstremoveele(lst,ele):
    n = len(ele)
    lstnew = lst
    for i in range(0,n):
        if len(lstnew) == 0:
            break
        lstnew.remove(ele[i])
    return lstnew

def edgesforeachnode(edges,nodes):
    nodesid = nodesosmid(nodes)
    starts = list()
    ends = list()
    Ned = len(edges)
    Nno = len(nodesid)
    for i in range(0,Ned):
        starts.append(str(edges[i]['start']))
        ends.append(str(edges[i]['end']))
    edfornd = dict()
    for i in range(0,Nno):
        node = nodesid[i]
        id_start = get_index(starts,node)
        id_end = get_index(ends,node)
        edge = list()
        for j in range(0,len(id_start)):
            edge.append(ends[id_start[j]])
        for j in range(0,len(id_end)):
            edge.append(starts[id_end[j]])
        edfornd[node] = edge
    return edfornd

def createnegedges(edges,nodes):
    edges_pos = edgesforeachnode(edges,nodes)
    nodesid = nodesosmid(nodes)
    N = len(nodesid)
    edges_neg = np.array([['start_ex','end_ex']])
    for i in range(0,N):
        nodesid1 = nodesid[:]
        edges_rt = lstremoveele(nodesid1,edges_pos[nodesid[i]])
        for j in range(0,len(edges_rt)):
            edges_neg = np.vstack((edges_neg,np.array([[nodesid[i],edges_rt[j]]])))
    edges_neg = np.delete(edges_neg,0,axis=0)
    return edges_neg

'''
def CreateNegativeSamples(edges_pos, nodes, nodes_embeddings):
    edges_neg = createnegedges(edges_pos,nodes)
    node = str(edges_pos[0]['start'])
    #relation = np.zeros(len(nodes_embeddings[node]))
    N = len(edges_neg)
    sample_neg = np.array([[nodes_embeddings[node],'0',nodes_embeddings[node]]],dtype=object)
    for i in range(0,N):
        start = edges_neg[i][0]
        end = edges_neg[i][1]
        rd = np.random.rand(1)
        if rd > 0.5:
            relation = '1'
        else:
            relation = '0'
        sample_neg = np.vstack((sample_neg,np.array([[nodes_embeddings[start],relation,nodes_embeddings[end]]],dtype=object)))
    sample_neg = np.delete(sample_neg,0,axis=0)
    return sample_neg

def insample(edges,sample):
    edges_sub = list()
    for i in range(0,len(edges)):
        if  edges[i][sample] == 1:
            edges_sub.append(edges[i])
    return edges_sub

def CreateTestSamples(edges_all, nodes_embeddings):
    #nodesid = nodesosmid(nodes)
    starts1 = list()
    ends1 = list()
    drivables = list()
    Ned1 = len(edges_all)
    edges_sub = insample(edges_all,'inSample1')
    for i in range(0,Ned1):
        starts1.append(str(edges_all[i]['start']))
        ends1.append(str(edges_all[i]['end']))
        drivables.append(edges_all[i]['drivable'])

    starts2 = list()
    ends2 = list()
    Ned2 = len(edges_sub)

    for i in range(0,Ned2):
        starts2.append(str(edges_sub[i]['start']))
        ends2.append(str(edges_sub[i]['end']))

    starts = list()
    ends = list()
    for i in range(0,Ned2):
        indt = get_index(starts1,starts2[i])
        indd = get_index(ends1,ends2[i])
        ind = list(set(indt).intersection(set(indd)))
        ind = ind[0]
        starts1.pop(ind)
        ends1.pop(ind)
        drivables.pop(ind)
    node = str(edges_sub[0]['start'])
    relation = np.zeros(len(nodes_embeddings[node]))
    sample_test = np.array([[nodes_embeddings[node],relation,nodes_embeddings[node]]],dtype=object)
    for i in range(0,len(starts1)):
        relation[0] = drivables[i]
        imd = np.array([[nodes_embeddings[starts1[i]],relation,nodes_embeddings[ends1[i]]]],dtype=object)
        sample_test = np.vstack((sample_test,imd))
    sample_test = np.delete(sample_test,0,axis=0)
    return sample_test
'''

def lstfindele(lst,ind):
    ele = list()
    for i in range(0,len(ind)):
        ele.append(lst[ind[i]])
    return ele

def createsamples(nodesset,edgesset):
    ## inputs: 
    ##        nodesset and edgesset for a graph, for example nodesset = ./train/sample/Chicago120nodesemb.npy, edgesset = ./train/sample/Chicago120edgesemb.npy
    ##         (nodesset[i].keys() = ['osmid','lat','lon','embeddings0','embeddings1','embeddings2'], edgesset[i].keys() = ['start','end','drivable','inSample','inSample2'])
    ## ouputs:
    ##        emb1: embeddings for relations in Sample1, where emb1[j] = [emb vector 1, '0', vector 2] or [emb vector 1, '1', emb vector 2]
    ##        emb2: embeddings for relations in Sample2, where emb2[j] = [emb vector 1, '0', vector 2] or [emb vector 1, '1', emb vector 2]
    ##        embn: embeddings for relations in negative sample(including every edge not in the original graph), where embn[j] = [emb vector 1, '0', emb vector 2] or [emb vector 1, '1', emb vector 2]
    ##        embt1: embeddings for relations in test sample for Sample1(including every edge not in Sample1 graph), where embt1[j] = [emb vector 1, '0', emb vector 2] or [emb vector 1, '1', emb vector 2]
    ##        embt2: embeddings for relations in test sample for Sample2(including every edge not in Sample2 graph), where embt2[j] = [emb vector 1, '0', emb vector 2] or [emb vector 1, '1', emb vector 2]
    ##        edgest1: edges corresponding to embt1, that is, edgest1[j]['start'] is the node id of emb vector 1 in embt1[j] and  edgest1[j]['end'] is the node id of emb vector 2 in embt1[j]
    ##        edgest2: edges corresponding to embt2, that is, edgest2[j]['start'] is the node id of emb vector 1 in embt1[j] and  edgest2[j]['end'] is the node id of emb vector 2 in embt2[j]
    nodes = list()
    for i in range(0,len(nodesset)):
        nodes.append(nodesset[i]['osmid'])
    starts = list()
    ends = list()
    starts1 = list()
    ends1 = list()
    starts2 = list()
    ends2 = list()
    emb1 = np.array([[nodesset[0]['embeddings0'],'0',nodesset[0]['embeddings0']]],dtype=object)
    emb2 = np.array([[nodesset[0]['embeddings0'],'0',nodesset[0]['embeddings0']]],dtype=object)
    for i in range(0,len(edgesset)):
        start = edgesset[i]['start']
        starts.append(start)
        end = edgesset[i]['end']
        ends.append(end)
        ind1 = nodes.index(start)
        ind2 = nodes.index(end)
        drivable = str(edgesset[i]['drivable'])
        if edgesset[i]['inSample1'] == 1:
            starts1.append(start)
            ends1.append(end)
            emb1 = np.vstack((emb1,np.array([[nodesset[ind1]['embeddings0'],drivable,nodesset[ind2]['embeddings0']]],dtype=object)))
        if edgesset[i]['inSample2'] == 1:
            starts2.append(start)
            ends2.append(end)
            emb2 = np.vstack((emb2,np.array([[nodesset[ind1]['embeddings0'],drivable,nodesset[ind2]['embeddings0']]],dtype=object)))
    emb1 = np.delete(emb1,0,axis=0)
    emb2 = np.delete(emb2,0,axis=1)
    embn = np.array([[nodesset[0]['embeddings0'],'0',nodesset[0]['embeddings0']]],dtype=object)
    embt1 = np.array([[nodesset[0]['embeddings0'],'0',nodesset[0]['embeddings0']]],dtype=object)
    embt2 = np.array([[nodesset[0]['embeddings0'],'0',nodesset[0]['embeddings0']]],dtype=object)
    edgest1 = list()
    edgest2 = list()
    for i in range(0,len(nodes)):
        node = nodes[i]

        ind1 = get_index(starts,node)
        ind2 = get_index(ends,node)
        ele1 = lstfindele(ends,ind1)
        ele2 = lstfindele(starts,ind2)
        ele = list(set(ele1).union(set(ele2)))
        nodesnew = nodes[i:len(nodes)]
        ele = list(set(nodesnew).intersection(set(ele)))
        edge = lstremoveele(nodesnew,ele)
        for j in range(0,len(edge)):
            start = nodes.index(node)
            end = nodes.index(edge[j])
            rd = np.random.rand(1)
            if rd < 0.7:
                drivable = '1'
            else:
                drivable = '0'
            embn = np.vstack((embn,np.array([[nodesset[start]['embeddings0'],drivable,nodesset[end]['embeddings0']]],dtype=object)))

        ind1 = get_index(starts1,node)
        ind2 = get_index(ends1,node)
        ele1 = lstfindele(ends1,ind1)
        ele2 = lstfindele(starts1,ind2)
        ele = list(set(ele1).union(set(ele2)))
        nodesnew = nodes[i:len(nodes)]
        ele = list(set(nodesnew).intersection(set(ele)))
        edge = lstremoveele(nodesnew,ele)
        for j in range(0,len(edge)):
            start = nodes.index(node)
            end = nodes.index(edge[j])
            rd = np.random.rand(1)
            edgest1.append({'start':node,'end':edge[j]})
            if rd < 0.7:
                drivable = '1'
            else:
                drivable = '0'
            embt1 = np.vstack((embt1,np.array([[nodesset[start]['embeddings0'],drivable,nodesset[end]['embeddings0']]],dtype=object)))

        ind1 = get_index(starts2,node)
        ind2 = get_index(ends2,node)
        ele1 = lstfindele(ends2,ind1)
        ele2 = lstfindele(starts2,ind2)
        ele = list(set(ele1).union(set(ele2)))
        nodesnew = nodes[i:len(nodes)]
        ele = list(set(nodesnew).intersection(set(ele)))
        edge = lstremoveele(nodesnew,ele)
        for j in range(0,len(edge)):
            start = nodes.index(node)
            end = nodes.index(edge[j])
            rd = np.random.rand(1)
            edgest2.append({'start':node,'end':edge[j]})
            if rd < 0.7:
                drivable = '1'
            else:
                drivable = '0'
            embt2 = np.vstack((embt2,np.array([[nodesset[start]['embeddings0'],drivable,nodesset[end]['embeddings0']]],dtype=object)))

    return emb1, emb2, embn, embt1, embt2, edgest1, edgest2



    

    


def gradsigm(x):
    z = 1.0/(1.0+np.exp(-x))
    return z*(1.0-z)

def scorefunction(emb1,emb2,w):
    sz = np.size(emb1,axis=0)
    if sz == 1:
       score = np.dot(emb1,w*emb2)
    elif sz > 1:
       score = np.zeros([sz])
       for i in range(0,sz):
           score[i] = np.dot(emb1[i],w*emb2[i])
    return score


def lossfunction(scorespos,scoresneg):
    npos = len(scorespos)
    nneg = len(scoresneg)
    loss = 0.0
    for i in range(0,npos):
        for j in range(0,nneg):
            loss = loss + max(scorespos[i]-scoresneg[j]+1,0.0)
    return loss

def gradloss(M,W,yemb1,yemb2,zemb1,zemb2,xyemb1,xyemb2,xzemb1,xzemb2):
    n1 = len(M)
    gdM = np.zeros([n1])
    m1 = len(yemb1[0])
    m2 = len(xyemb1[0])
    gdW = np.zeros([m1,m2])
    k1 = len(yemb1)
    k2 = len(zemb1)
    score1 = scorefunction(yemb1,yemb2,M)
    score2 = scorefunction(zemb1,zemb2,M)
    #imd = score1-score2+1.0
    for i in range(0,n1):
        for j in range(0,k1):
            for k in range(0,k2):
                #score1 = scorefunction(yemb1[j],yemb2[j],M)
                #score2 = scorefunction(zemb1[k],kemb2[k],M)
                imd = score1[j]-score2[k]+1.0
                if imd >= 0:
                        
                    gdM[i] = gdM[i] + imd*(yemb1[j][i]*yemb2[j][i]-zemb1[k][i]*zemb2[k][i])

    for i in range(0,m1):
        for j in range(0,m2):
            for k in range(0,k1):
                for l in range(0,k2):
                    #for m in range(0,n1):
                    imd = score1[k]-score2[l]+1.0
                    if imd >= 0.0:
                        #print(imd)
                        y1 = gradsigm(np.dot(W[i,:],xyemb1[k]))
                        y2 = gradsigm(np.dot(W[i,:],xyemb2[k]))
                        z1 = gradsigm(np.dot(W[i,:],xzemb1[l]))
                        z2 = gradsigm(np.dot(W[i,:],xzemb2[l]))
                        gdW[i][j] = gdW[i][j] + imd*(M[i]*yemb1[k][i]*y2*xyemb2[k][j] + M[i]*yemb2[k][i]*y1*xyemb1[k][j] - M[i]*zemb1[l][i]*z2*xzemb2[l][j] - M[i]*zemb2[l][i]*z1*xzemb1[l][j])
    return gdM, gdW

def sgd(v,g,u):
    return v-u*g

def randsample(n,k):
    x = list()
    for i in range(0,k):
        m = random.randit(0,n-1)
        if m not in x:
            x.append(m)
    return x

def newparameters(M1,gdM1,M2,gdM2,W,gdW,lr):
    M11 = sgd(M1,gdM1,lr)
    M21 = sgd(M2,gdM2,lr)
    W1 = sgd(W,gdW,lr)
    return M11, M21, W1

def distmult(X,Y,W0,M10,M20,lr,batch1,batch2,epoch):
    n = int(np.floor(len(X)/batch1))
    n1 = int(np.floor(len(Y)/batch2))
    nx = len(X)
    nn = len(X[0][0])
    nx1 = len(Y)
    sizes = list()
    sizes1 = list()
    fullind = np.linspace(0,nx-1,nx,dtype=int)
    fullind1 = np.linspace(0,nx1-1,nx1,dtype=int)
    for i in range(0,n):
        if i < n-1:
           ind = np.random.randint(0,len(fullind),batch1,dtype=int)
        else:
           ind = np.linspace(0,len(fullind)-1,len(fullind),dtype=int)
        sizes.append(fullind[ind])
        fullind = np.delete(fullind,ind)
    for i in range(0,n1):
        if i < n1-1:
           ind = np.random.randint(0,len(fullind1),batch2,dtype=int)
        else:
           ind = np.linspace(0,len(fullind1)-1,len(fullind1),dtype=int)
        sizes1.append(fullind1[ind])
        fullind1 = np.delete(fullind1,ind)
    #print(sizes,sizes1)
    Xt = list()
    Yt = list()
    for i in range(0,n):
        Xt.append(X[sizes[i]])
    for i in range(0,n1):
        Yt.append(Y[sizes1[i]])
    W = W0
    M1 = M10
    M2 = M20
    #Wt = np.transpose(W)
    for i in range(0,epoch):
        for j in range(0,n):
            X1 = np.hstack((Xt[j][0][0],Xt[j][0][2]))
            X2 = np.hstack((Xt[j][0][0],Xt[j][0][2]))
            Y1 = np.hstack((Yt[j][0][0],Yt[j][0][2]))
            Y2 = np.hstack((Yt[j][0][0],Yt[j][0][2]))
            for k in range(0,len(Xt[j])):
                if Xt[j][k][1] == '0':
                    X1 = np.vstack((X1,np.hstack((Xt[j][k][0],Xt[j][k][2]))))
                elif Xt[j][k][1] == '1':
                    X2 = np.vstack((X2,np.hstack((Xt[j][k][0],Xt[j][k][2]))))
            for k in range(0,len(Yt[j])):
                if Yt[j][k][1] == '0':
                    Y1 = np.vstack((Y1,np.hstack((Yt[j][k][0],Yt[j][k][2]))))
                elif Yt[j][k][1] == '1':
                    Y2 = np.vstack((Y2,np.hstack((Yt[j][k][0],Yt[j][k][2]))))
            if len(X1) == 1:
                ind1x = 1
            elif len(X2) == 1:
                ind2x = 1
            if len(Y1) == 1:
                ind1y = 1
            elif len(Y2) == 1:
                ind2y = 1
            X1 = np.delete(X1,0,axis=0)
            X2 = np.delete(X2,0,axis=0)
            Y1 = np.delete(Y1,0,axis=0)
            Y2 = np.delete(Y2,0,axis=0)
            Wt = np.transpose(W)
            WY11 = Y1[:,0:nn]
            WY11 = sigmoid(np.dot(WY11,Wt))
            WY21 = Y1[:,nn:2*nn]
            WY21 = sigmoid(np.dot(WY21,Wt))
            WX11 = X1[:,0:nn]
            WX11 = sigmoid(np.dot(WX11,Wt))
            WX21 = X1[:,nn:2*nn]
            WX21 = sigmoid(np.dot(WX21,Wt))
            WY12 = Y2[:,0:nn]
            WY12 = sigmoid(np.dot(WY12,Wt))
            WY22 = Y2[:,nn:2*nn]
            WY22 = sigmoid(np.dot(WY22,Wt))
            WX12 = X2[:,0:nn]
            WX12 = sigmoid(np.dot(WX12,Wt))
            WX22 = X2[:,nn:2*nn]
            WX22 = sigmoid(np.dot(WX22,Wt))

            scores1pos = scorefunction(WX11,WX21,M1)
            scores2pos = scorefunction(WX12,WX22,M2)
            scores1neg = scorefunction(WY11,WY21,M1)
            scores2neg = scorefunction(WY12,WY22,M2)
            loss1 = lossfunction(scores1pos,scores1neg)
            loss2 = lossfunction(scores2pos,scores2neg)
            #print(Y1[:,0:nn],X1[:,0:nn])
            #gradloss(M,W,yemb1,yemb2,zemb1,zemb2,xyemb1,xyemb2,xzemb1,xzemb2)
            [gdM1, gdW1] = gradloss(M1,W,WY11,WY21,WX11,WX21,Y1[:,0:nn],Y1[:,nn:2*nn],X1[:,0:nn],X1[:,nn:2*nn])
            #gradloss(M,W,yemb1,yemb2,zemb1,zemb2,xyemb1,xyemb2,xzemb1,xzemb2)
            [gdM2, gdW2] = gradloss(M2,W,WY12,WY22,WX12,WX22,Y2[:,0:nn],Y2[:,nn:2*nn],X2[:,0:nn],X2[:,nn:2*nn])
            gdW = (gdW1+gdW2)/2.0
            #newparameters(M1,gdM1,M2,gdM2,W,gdW,lr):
            [M1, M2, W] = newparameters(M1,gdM1,M2,gdM2,W,gdW,lr)
            #print('M1=',gdM1,'M2=',gdM2,'W=',gdW)
        print('EPOCH ',i,': loss1: ',loss1,' loss2: ',loss2,' loss: ',loss1+loss2)

    return W, M1, M2
    
def sigmoid(z):
    return 1.0/(1+np.exp(-z))

def predict(M1,M2,W,X,e):
    Wt = np.transpose(W)
    edges = list()
    Y = np.hstack((X[0][0],X[0][2]))
    n = len(X[0][0])
    for i in range(0,len(X)):
        Y = np.vstack((Y,np.hstack((X[i][0],X[i][2]))))
    Y = np.delete(Y,0,axis=0)
    X1 = Y[:,0:n]
    X2 = Y[:,n:2*n]
    emb1 = sigmoid(np.dot(X1,Wt))
    emb2 = sigmoid(np.dot(X2,Wt))
    s1 = sigmoid(scorefunction(emb1,emb2,M1))
    s2 = sigmoid(scorefunction(emb1,emb2,M2))
    s = s1 + s2
    for i in range(0,len(X)):
        if s[i] < e:
            edges.append('none')
        elif s1[i] > s2[i]:
            edges.append(['0',s1[i]])
        else:
            edges.append(['1',s2[i]])
    return edges



