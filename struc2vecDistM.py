# -*- coding: utf-8 -*-
"""
Created on Wed Apr  8 18:29:41 2020

@author: Qiyuan Pang
"""


import json
import os
import platform
import networkx as nx
import matplotlib.pyplot as plt
from struc2vec import *
from sklearn.metrics import brier_score_loss, log_loss
from scipy.special import expit
from ampligraph.latent_features.models import DistMult
from auxiliaryfunc import * 

### create edgelist files 
filepath = './train'
#filenames = os.listdir(filepath)
filepathemb = filepath + '/emb'
folder = os.path.exists(filepathemb)
if not folder:
   os.makedirs(filepathemb)
filepath1 = filepath + '/sample'
folder1 = os.path.exists(filepath1)
if not folder1:
   os.makedirs(filepath1)
filepath2 = filepath + '/sample1'
folder2 = os.path.exists(filepath2)
if not folder2:
   os.makedirs(filepath2)
filepath3 = filepath + '/sample2'
folder3 = os.path.exists(filepath3)
if not folder3:
   os.makedirs(filepath3)

filenames = os.listdir(filepath)
filenames.remove('emb')
filenames.remove('sample')
filenames.remove('sample1')
filenames.remove('sample2')
N = len(filenames)
N2 = int(N/2)
samples_edge = list()

while len(filenames) > 0:
    name = filenames[0]
    #samples.append(name[0:len(name)-10])
    edgefile = filepath + '/' + name[0:len(name)-10] + 'edges.json'
    nodefile = filepath + '/' + name[0:len(name)-10] + 'nodes.json'
    with open(edgefile, 'r') as f:
         edges = json.load(f)
    f.close()
    #samples_edge.append({'name':name[0:len(name)-10],'edges':edges})
    with open(nodefile, 'r') as f:
         nodes = json.load(f)
    f.close()
    samples_edge.append({'name':name[0:len(name)-10],'edges':edges,'nodes':nodes})
    G = nx.Graph()
    #print(edgefile,len(filenames))
    #print(nodefile)
    for i in range(0,len(nodes)):
        #print(nodes[i])
        G.add_node(nodes[i]['osmid'])
        G.nodes[nodes[i]['osmid']]['lon'] = nodes[i]['lon']
        G.nodes[nodes[i]['osmid']]['lat'] = nodes[i]['lat']
    for i in range(0,len(edges)):
        G.add_edge(edges[i]['start'],edges[i]['end'])
        G.edges[edges[i]['start'],edges[i]['end']]['drivable'] = edges[i]['drivable']
        #G.edges[edges[i]['start'],edges[i]['end']]['inSample1'] = edges[i]['inSample1']
        #G.edges[edges[i]['start'],edges[i]['end']]['inSample2'] = edges[i]['inSample2']
        
    filesave = filepathemb + '/' + name[0:len(name)-10] + 'sample0' + '.edgelist'
    nx.write_edgelist(G,filesave)
    
    G1 = nx.Graph()
    for i in range(0,len(nodes)):
        #print(nodes[i])
        G1.add_node(nodes[i]['osmid'])
        G1.nodes[nodes[i]['osmid']]['lon'] = nodes[i]['lon']
        G1.nodes[nodes[i]['osmid']]['lat'] = nodes[i]['lat']
    for i in range(0,len(edges)):
        if  edges[i]['inSample1'] == 1:
            G1.add_edge(edges[i]['start'],edges[i]['end'])
            G1.edges[edges[i]['start'],edges[i]['end']]['drivable'] = edges[i]['drivable']
    filesave1 = filepathemb + '/' + name[0:len(name)-10] + 'sample1' + '.edgelist'
    nx.write_edgelist(G1,filesave1)

    G2 = nx.Graph()
    for i in range(0,len(nodes)):
        #print(nodes[i])
        G2.add_node(nodes[i]['osmid'])
        G2.nodes[nodes[i]['osmid']]['lon'] = nodes[i]['lon']
        G2.nodes[nodes[i]['osmid']]['lat'] = nodes[i]['lat']
    for i in range(0,len(edges)):
        if  edges[i]['inSample2'] == 1:
            G2.add_edge(edges[i]['start'],edges[i]['end'])
            G2.edges[edges[i]['start'],edges[i]['end']]['drivable'] = edges[i]['drivable']
    filesave2 = filepathemb + '/' + name[0:len(name)-10] + 'sample2' + '.edgelist'
    nx.write_edgelist(G2,filesave2)

    filenames.remove(name[0:len(name)-10] + 'edges.json')
    filenames.remove(name[0:len(name)-10] + 'nodes.json')

print('struc2vec starts:')

filepath_emb = filepath + '/emb'
filenames_emb = os.listdir(filepath_emb)
N_emb = len(filenames_emb)
embed = list()
embed1 = list()
embed2 = list()
samples_emb = list()
samples_emb1 = list()
samples_emb2 = list()
names = list()
for i in range(0,len(samples_edge)):
    names.append(samples_edge[i]['name'])
#print(names)
file_s1 = filepath + '/sample1'
file_s2 = filepath + '/sample2'
file_s = filepath + '/sample'
while len(filenames_emb) > 0:
    #print(filenames)
    emb = filenames_emb[i]
    name = emb[0:len(emb)-16]
    edgelist_emb0 = filepath_emb + '/' + name + 'sample0.edgelist'
    edgelist_emb1 = filepath_emb + '/' + name + 'sample1.edgelist'
    edgelist_emb2 = filepath_emb + '/' + name + 'sample2.edgelist'
    G0 = nx.read_edgelist(edgelist_emb0, create_using = nx.DiGraph(), nodetype = None)
    G1 = nx.read_edgelist(edgelist_emb1, create_using = nx.DiGraph(), nodetype = None)
    G2 = nx.read_edgelist(edgelist_emb2, create_using = nx.DiGraph(), nodetype = None)
    model0 = Struc2Vec(G0,10,80,workers=4,verbose=10)
    model1 = Struc2Vec(G1,10,80,workers=4,verbose=10)
    model2 = Struc2Vec(G2,10,80,workers=4,verbose=10)
    model0.train(embed_size = 50, window_size=5,iter=3)
    model1.train(embed_size = 50, window_size=5,iter=3)
    model2.train(embed_size = 50, window_size=5,iter=3)
    nodes0 = model0.get_embeddings()
    nodes1 = model1.get_embeddings()
    nodes2 = model2.get_embeddings()
    edges = samples_edge[names.index(name)]['edges']
    nodes = samples_edge[names.index(name)]['nodes']
    N_edges = len(edges)
    imd = np.array([1])
    relation = np.zeros(len(nodes0[str(edges[0]['start'])]))
       
    count = 0
    for j in range(0,N_edges):
        if edges[j]['inSample1'] == 1:
           start = edges[j]['start']
           end = edges[j]['end']
           drivable = edges[j]['drivable']
           relation[0] = drivable
           dr = str(drivable)
           #if  start in nodes1.keys() and end in nodes1.keys():
           if  count == 0:
               emb_imd = np.array([[nodes1[str(start)],dr,nodes1[str(end)]]],dtype=object)
           emb_imd = np.vstack((emb_imd,np.array([[nodes1[str(start)],dr,nodes1[str(end)]]],dtype=object)))
           count = count + 1
               #emb_imd = np.vstack((emb_imd,np.array([nodes[start],np.array([drivable]),nodes[end]])))
        #samples_emb1.append(filenames_emb[i][0:len(filenames_emb[i])-10])
        #embed1.append(model.get_embeddings())
    s = file_s1 + '/' + name + '.npy'
    emb_imd = np.delete(emb_imd,0,axis=0)
    np.save(s,emb_imd)
                
    count = 0
    for j in range(0,N_edges):
        #print(edges[j])
        if edges[j]['inSample2'] == 1:
           start = edges[j]['start']
           end = edges[j]['end']
           drivable = edges[j]['drivable']
           relation[0] = drivable
           dr = str(drivable)
           #if  start in nodes2.keys() and end in nodes2.keys(): 
           if  count == 0:
               emb_imd = np.array([[nodes2[str(start)],dr,nodes2[str(end)]]],dtype=object)
           emb_imd = np.vstack((emb_imd,np.array([[nodes2[str(start)],dr,nodes2[str(end)]]],dtype=object)))
           count = count + 1
        #samples_emb2.append(filenames_emb[i][0:len(filenames_emb[i])-10])
        #embed2.append(model.get_embeddings())
    s = file_s2 + '/' + name + '.npy'
    emb_imd = np.delete(emb_imd,0,axis=0)
    np.save(s,emb_imd)
    
    count = 0
    for j in range(0,N_edges):
        start = edges[j]['start']
        end = edges[j]['end']
        drivable = edges[j]['drivable']
        relation[0] = drivable
        dr = str(drivable)
        #emb_imd = np.vstack((emb_imd,np.array([nodes[start],np.array([drivable]),nodes[end]])))
        #if  start in nodes0.keys() and end in nodes0.keys(): 
        if  count == 0:
            emb_imd = np.array([[nodes0[str(start)],dr,nodes0[str(end)]]],dtype=object)
        emb_imd = np.vstack((emb_imd,np.array([[nodes0[str(start)],dr,nodes0[str(end)]]],dtype=object)))
        count = count + 1
        #samples_emb.append(filenames_emb[i][0:len(filenames_emb[i])-10])
        #embed.append(model.get_embeddings())
    s = file_s + '/' + name + '.npy'
    emb_imd = np.delete(emb_imd,0,axis=0)
    np.save(s,emb_imd)
    
    emb_imd_neg = CreateNegativeSamples(edges, nodes, nodes0)
    s_neg = file_s + '/' + name + 'NEG.npy'
    np.save(s_neg,emb_imd_neg)
    emb_imd_test = CreateTestSamples(edges,nodes0)
    s_test = file_s + '/' + name + 'TEST.npy'
    np.save(s_test,emb_imd_test)
    '''
    keys1 = nodes1.keys()
    keys2 = nodes2.keys()
    for i in range(0,len(nodes)):
        osmid = str(nodes[i]['osmid'])
        emb0 = nodes0[osmid]
        emb1 = []
        if osmid in keys1:
            emb1 = nodes1[osmid]
        emb2 = []
        if osmid in keys2:
            emb2 = nodes2[osmid]
        nodes[i]['embeddings0'] = list(emb0)
        nodes[i]['embeddings1'] = list(emb1)
        nodes[i]['embeddings2'] = list(emb2)
    emb0 = list(emb0)
    print(type(emb0[0]),len(emb0))
    s = file_s + '/' + name + 'nodesemb.npy'
    np.save(s,nodes)
    s = file_s + '/' + name + 'edgesemb.npy'
    np.save(s,edges)
    print((name + 'ebeddings saved!' + str(len(filenames_emb)) + 'size:' + str(len(emb0)) + ' resting ================================================================='))
    '''


    filenames_emb.remove(name + 'sample0.edgelist')
    filenames_emb.remove(name + 'sample1.edgelist')
    filenames_emb.remove(name + 'sample2.edgelist')



#model = DistMult(batches_count=64, seed=0, epochs=500, k=100, eta=20, optimizer='adam', optimizer_params={'lr':0.0001}, loss='pairwise', verbose=True)

#model.fit(embeddings1[0])

#scores = model.predict(embeddings[0])

